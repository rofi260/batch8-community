
import java.util.Scanner;
class Test {
    public static void main( String[] args) {
        Scanner inputan = new Scanner(System.in);
        String perintah = inputan.nextLine();
        String kode = "java CharacterizedString";

        if (perintah.equals(kode)) {

            System.out.print("masukan kata/kalimat yang ingin di pecahkan menjadi karakter :");
            String kalimat = inputan.nextLine();
            System.out.println("===============================================");
            System.out.println("String penuh: " + kalimat);
            System.out.println("===============================================");
            System.out.println(
                    "Hasil : ");

            for (int i = 0; i < kalimat.length(); i++) {
                int nitip = i + 1;
                System.out.print("Karakter[" + nitip + "]: " + kalimat.charAt(i) + "\n");
            }

            System.out.println("===============================================");

        }
        else{
            System.out.println("kode salah");
        }
    }
}